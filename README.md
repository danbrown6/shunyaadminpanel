# Shunya Admin Panel
Shunya is a C#-based remote administration tool that offers rapid performance and a minimal footprint. Its applications span from assisting users and handling routine administrative tasks to monitoring employees. With its exceptional stability and user-friendly interface, Shunya presents an ideal solution for remote administration needs.

## Features
* System Information
* Task Manager
* File Manager
* Startup Manager
* Remote Desktop
* Remote Shell
* Remote Registry Editor
* Reverse Proxy (SOCKS5)
* TCP network stream (IPv4 & IPv6 support)
* Encrypted communication (TLS)

## Supported runtimes and operating systems
* .NET Framework 4.5.2 or higher
* Supported operating systems (32- and 64-bit)
  * Windows 11
  * Windows Server 2022
  * Windows 10
  * Windows Server 2019
  * Windows Server 2016
  * Windows 8/8.1
  * Windows Server 2012
  * Windows 7
  * Windows Server 2008 R2

## Compiling
To begin working on the project called `ShunyaServer.sln`, launch Visual Studio 2019 or a later version, ensuring that you have installed the required .NET desktop development features. Also, make sure to [restore the NuGET packages](https://docs.microsoft.com/en-us/nuget/consume-packages/package-restore) as described in the documentation. After successfully installing all the packages, you can compile the project by either clicking on the `Build` option located at the top menu or by pressing the `F6` key. The resulting executable files will be available in the `Bin` directory. Please refer to the instructions below to select the appropriate build configuration.

## Building a client
| Build configuration         | Usage scenario | Description
| ----------------------------|----------------|--------------
| Debug configuration         | Testing        | The pre-defined [Settings.cs](/Shunya.Client/Config/Settings.cs) will be used, so edit this file before compiling the client. You can execute the client directly with the specified settings.
| Release configuration       | Production     | Start `ShunyaServer.exe` and use the client builder.

## License
Shunya Admin Panel is distributed under the [MIT License].  

## Feedback
I really appreciate all kinds of feedback and contributions.
