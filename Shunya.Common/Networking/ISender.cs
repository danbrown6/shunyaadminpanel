﻿using Shunya.Saman.Messages;

namespace Shunya.Saman.Networking
{
    public interface ISender
    {
        void Send<T>(T message) where T : IMessage;
        void Disconnect();
    }
}
