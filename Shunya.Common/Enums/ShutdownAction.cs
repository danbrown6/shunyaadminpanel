﻿namespace Shunya.Saman.Enums
{
    public enum ShutdownAction
    {
        Shutdown,
        Restart,
        Standby
    }
}
