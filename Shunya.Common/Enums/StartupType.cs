﻿namespace Shunya.Saman.Enums
{
    public enum StartupType
    {
        LocalMachineRun,
        LocalMachineRunOnce,
        CurrentUserRun,
        CurrentUserRunOnce,
        StartMenu
    }
}
