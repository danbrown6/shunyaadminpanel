﻿namespace Shunya.Saman.Enums
{
    public enum AccountType
    {
        Admin,
        User,
        Guest,
        Unknown
    }
}
