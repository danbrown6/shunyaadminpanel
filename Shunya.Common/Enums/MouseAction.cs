﻿namespace Shunya.Saman.Enums
{
    public enum MouseAction
    {
        LeftDown,
        LeftUp,
        RightDown,
        RightUp,
        MoveCursor,
        ScrollUp,
        ScrollDown,
        None
    }
}
