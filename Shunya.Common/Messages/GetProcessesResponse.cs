﻿using ProtoBuf;
using Shunya.Saman.Models;

namespace Shunya.Saman.Messages
{
    [ProtoContract]
    public class GetProcessesResponse : IMessage
    {
        [ProtoMember(1)]
        public Process[] Processes { get; set; }
    }
}
