﻿using ProtoBuf;
using Shunya.Saman.Enums;

namespace Shunya.Saman.Messages
{
    [ProtoContract]
    public class SetUserStatus : IMessage
    {
        [ProtoMember(1)]
        public UserStatus Message { get; set; }
    }
}
