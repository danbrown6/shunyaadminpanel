﻿using ProtoBuf;
using Shunya.Saman.Models;

namespace Shunya.Saman.Messages
{
    [ProtoContract]
    public class GetDrivesResponse : IMessage
    {
        [ProtoMember(1)]
        public Drive[] Drives { get; set; }
    }
}
