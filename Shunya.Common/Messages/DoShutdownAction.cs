﻿using ProtoBuf;
using Shunya.Saman.Enums;

namespace Shunya.Saman.Messages
{
    [ProtoContract]
    public class DoShutdownAction : IMessage
    {
        [ProtoMember(1)]
        public ShutdownAction Action { get; set; }
    }
}
