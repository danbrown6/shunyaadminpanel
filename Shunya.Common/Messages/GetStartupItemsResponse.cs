﻿using ProtoBuf;
using Shunya.Saman.Models;
using System.Collections.Generic;

namespace Shunya.Saman.Messages
{
    [ProtoContract]
    public class GetStartupItemsResponse : IMessage
    {
        [ProtoMember(1)]
        public List<StartupItem> StartupItems { get; set; }
    }
}
