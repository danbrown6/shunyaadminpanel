﻿using ProtoBuf;
using Shunya.Saman.Models;

namespace Shunya.Saman.Messages
{
    [ProtoContract]
    public class DoChangeRegistryValue : IMessage
    {
        [ProtoMember(1)]
        public string KeyPath { get; set; }

        [ProtoMember(2)]
        public RegValueData Value { get; set; }
    }
}
