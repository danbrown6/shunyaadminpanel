﻿namespace Shunya.Server.Enums
{
    public enum TransferType
    {
        Upload,
        Download
    }
}
